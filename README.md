## Floppy lab

En el marco de [Flash Party 2021](https://flashparty.rebelion.digital/) nació floppy.rlab.be, una iniciativa hackartivista anti-obsolescencia programada que invoca a la reflexión colectiva sobre el arte digital como vehículo político para plantear nuevas formas de consumo y producción, nuevas formas de vinculación tecno-culturales, en apenas unos megabytes de espacio.

![imagen del diskette con calcomanía logo rlab](./public/assets/img/floppy_rlab.jpg)

Pedí tu floppy en el site mencionado y recolectá estos bits del hacklab!

- [Landing floppy rlab](https://floppy.rlab.be/)
- [Entrada en wiki](https://wiki.rlab.be/es/floppy-rlab)
- [Pedir por Telegram](https://t.me/joinchat/yplxJGHHLYIyZTNh)
- [Video presentado en Flash Party](https://gitlab.com/rlyehlab/floppylab/-/raw/master/public/assets/v2-edit-rlab-floppy.mp4)