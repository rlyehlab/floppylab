# 1. pone al lado de este script un archivo "text.txt" con el texto a encuadrar
# 2. configura las variables de aca abajo como mas te guste
# 3. ejecuta el script: python3 ascii-box.py
# Un ejemplo de resultado se puede ver en "manifiesto-en-consola.jpg"

TOP_CHAR='_'
TOP_ESQUINA_CHAR='+'
LATERAL_CHAR='x'
BOTTOM_CHAR='#'
BOTTOM_ESQUINA_CHAR='@'
MARGIN_CHAR='~'
PADDING_CHAR=' '

X_PADDING=3
Y_PADDING=2
X_MARGIN=5
INNER_WIDTH=90

with open('text.txt', 'r') as text_file:
  text_lines = [l.strip() for l in text_file.readlines()]
  # primera linea (margin+esquina+padding+top+padding+esquina+margin)
  print( \
    X_MARGIN*MARGIN_CHAR + \
    TOP_ESQUINA_CHAR + \
    X_PADDING*TOP_CHAR + \
    INNER_WIDTH*TOP_CHAR + \
    X_PADDING*TOP_CHAR + \
    TOP_ESQUINA_CHAR + \
    X_MARGIN*MARGIN_CHAR \
  )
  # lineas de padding top
  for i in range(Y_PADDING):
    print( \
      X_MARGIN*MARGIN_CHAR + \
      LATERAL_CHAR + \
      X_PADDING*PADDING_CHAR + \
      INNER_WIDTH*PADDING_CHAR + \
      X_PADDING*PADDING_CHAR + \
      LATERAL_CHAR + \
      X_MARGIN*MARGIN_CHAR \
    )
  for line in text_lines:
    # si la lonea del texto entra en una linea de la caja
    if len(line) <= INNER_WIDTH:
      print( \
        X_MARGIN*MARGIN_CHAR + \
        LATERAL_CHAR + \
        X_PADDING*PADDING_CHAR + \
        line.ljust(INNER_WIDTH, PADDING_CHAR) + \
        X_PADDING*PADDING_CHAR + \
        LATERAL_CHAR + \
        X_MARGIN*MARGIN_CHAR \
      )
    else:
      # sino rompemos la linea de texto en varias lineas de la caja
      line_break_i = 0
      while len(line[line_break_i:]) > INNER_WIDTH+1:
        print( \
          X_MARGIN*MARGIN_CHAR + \
          LATERAL_CHAR + \
          X_PADDING*PADDING_CHAR + \
          line[line_break_i:line_break_i+INNER_WIDTH-1] + '-' + \
          X_PADDING*PADDING_CHAR + \
          LATERAL_CHAR + \
          X_MARGIN*MARGIN_CHAR \
        )
        line_break_i += INNER_WIDTH-1
      # para la ultima linea del texto usamos la misma formula que 
      # el if the arriba
      print( \
        X_MARGIN*MARGIN_CHAR + \
        LATERAL_CHAR + \
        X_PADDING*PADDING_CHAR + \
        line[line_break_i:].ljust(INNER_WIDTH, PADDING_CHAR) + \
        X_PADDING*PADDING_CHAR + \
        LATERAL_CHAR + \
        X_MARGIN*MARGIN_CHAR \
      )
    #end if len(line)
  #end for line
  # lineas de padding bottom
  for i in range(Y_PADDING):
    print( \
      X_MARGIN*MARGIN_CHAR + \
      LATERAL_CHAR + \
      X_PADDING*PADDING_CHAR + \
      INNER_WIDTH*PADDING_CHAR + \
      X_PADDING*PADDING_CHAR + \
      LATERAL_CHAR + \
      X_MARGIN*MARGIN_CHAR \
    )
  # ultima linea
  print( \
    X_MARGIN*MARGIN_CHAR + \
    BOTTOM_ESQUINA_CHAR + \
    X_PADDING*BOTTOM_CHAR + \
    INNER_WIDTH*BOTTOM_CHAR + \
    X_PADDING*BOTTOM_CHAR + \
    BOTTOM_ESQUINA_CHAR + \
    X_MARGIN*MARGIN_CHAR \
  )
