Manifiesto rlab

rlab apunta a hackear la programacion subjetiva
alterar intervenir y collagear el codigo-arte.

Somos hackers, artivistas y activistas,
construyendo en pos de tecnologias
politicas anticapitalistas inclusivas,
compartiendo espacios virtuales
y no-virtuales abiertxs a participaciones
activas y comprometidas.

Seremos cyborgs vestidxs de rojo y negro, cables
y placas; cyborgs reflexionando
y tecleando nuevos codigos para revertir los efectos
de un mundo lleno de virtualidades.

Queremos una liberacion politica-cultural y una
tecnologia social al servicio critico
de nuestras camaradas del mundo.

Queremos glitchear el sistema para demostrar que
las respuestas que nos quiere dar
no son las unicas ni las mejores; sabotearemos
la marea de lo hegemonico. Estamxs dispuestxs
a incluir, commitear y mergear proyectos entre
todxs, con codigos de convivencia copyleft.

No queremos un espacio forzado por una moneda
empresarial y explotadora.

No queremos ser un dato mas que se vigile.
Cuestionaremos el poder, la internet y el capital.
Lanzaremos los cables de red necesarios para
construir comunidades P2P.

Propondremos y teclearemos otro software:
malicioso, obrero y antisistema.
Crearemos bardo. Ese es nuestro commit.

Sostendremos hackeando y pusheando un espacio de
**Resistencia, Libertad, Autonomia y Bytes**

## ~ $ ./rlab

[Manifiesto rlab](https://manifiesto.rlab.be)  
[sitio principal rlab](https://rlab.be)  