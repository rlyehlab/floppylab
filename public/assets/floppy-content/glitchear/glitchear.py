## uso:
## python glitchear.py <letras a glitchear> <frase larga a glitchear ...>
## python glitchear.py 2 hola hola
## oneliner RLAB [ soporta live reload :p ]:
## while true; do python glitchear.py 8 --- Resistencia, libertad, autonomía y bytes ---; sleep 0.1; done

import sys, random, math
##https://www.geeksforgeeks.org/print-colors-python-terminal/
from colorama import init, Fore, Back, Style

DEBUG=False
#Fore: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
#Back: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
COLOR_TEXT=Fore.BLACK
COLOR_BG=Back.GREEN
#COLOR_TEXT=Fore.WHITE
#COLOR_BG=Back.CYAN

# use Colorama to make Termcolor work on Windows too
init()

DEBUG and print('Start!')

#chars = "~!@#$%^&*()_+`1234567890-=[]\\{}|;':\",./<>?ç¿¶´«»¬¥’‘̛^€¤³²¹"
chars = "~!@#$%^&*()_+/<>?¿"

input_rand = int(sys.argv[1])
input_str = ' '.join(sys.argv[2:])

rand_chars = input_rand
output_str = input_str
replaced = {}

if len(input_str) < rand_chars: rand_chars = len(input_str)

DEBUG and print('Replacing', rand_chars, 'chars of', len(input_str))

while len(replaced) < rand_chars:
  r = math.floor( random.random() * (len(input_str)) )
  if r in replaced: continue
  r_char = math.floor( random.random() * (len(chars)) )
  ## TypeError: 'str' object does not support item assignment
  ##output_str[r] = chars[r_char]
  ## https://thispointer.com/python-replace-character-in-string-by-index-position/
  output_str = output_str[0:r] + chars[r_char] + output_str[r+1:]
  replaced[r] = True
  DEBUG and print('Replaced char', r, 'with', chars[r_char])

DEBUG and print('Output:')
print(COLOR_TEXT + COLOR_BG + output_str)

DEBUG and print('Done!')

sys.exit(0)
